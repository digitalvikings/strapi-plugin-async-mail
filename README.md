# Strapi plugins

This repository contains several plugins and a Strapi instance for development. (The name of the repository will probably be 
renamed at some poin)

All plugins share one .env file. All possible settings are in the [.env.example](.env.example).

## Installation

```
git clone https://gitlab.com/digitalvikings/strapi-plugin-async-mail.git
cd strapi-plugin-async-mail
yarn
yarn build
yarn dev
```

Plugins

- [anonymize](src/plugins/anonymize/)
- [audit-log](src/plugins/audit-log/)
- [auto-uuid](src/plugins/auto-uuid/)
- [async-mail](src/plugins/async-mail)
- [bfa](src/plugins/bfa)

