module.exports = ({ env }) => ({
  "async-mail": {
    enabled: true,
    resolve: "./src/plugins/async-mail",
    config: {
      scanLimit: env.int("ASYNC_MAIL_SCAN_LIMIT", 10),
      recipient: env("ASYNC_MAIL_RECIPIENT"),
    },
  },
  email: {
    config: {
      provider: "nodemailer",
      providerOptions: {
        host: env("EMAIL_SMTP_HOST", "mc.cross-solution.de"), //SMTP Host
        port: env("EMAIL_SMTP_PORT", 465), //SMTP Port
        imapHost: env("EMAIL_IMAP_HOST", env("EMAIL_SMTP_HOST", "")),
        imapPort: env("EMAIL_IMAP_PORT", 993),
        secure: env("EMAIL_SMTP_SECURE", true),
        auth: {
          user: env("EMAIL_SMTP_USERNAME", ""),
          pass: env("EMAIL_SMTP_PASSWORD", ""),
          imapUser: env("EMAIL_IMAP_USERNAME", env("EMAIL_SMTP_USERNAME", "")),
          imapPass: env("EMAIL_IMAP_PASSWORD", env("EMAIL_SMTP_PASSWORD", "")),
        },
        rejectUnauthorized: true,
        requireTLS: true,
      },
      settings: {
        defaultFrom: env("EMAIL_SETTINGS_FROM", ""),
        defaultReplyTo: env("EMAIL_SETTINGS_REPLYTO", ""),
        reminderDelay: env.int("EMAIL_REMINDER_DELAY", 300),
        confirmEmailUrl: env(
          "CONFIRM_EMAIL_URL",
          "http://localhost:9000/#/confirm-email/",
        ),
      },
    },
  },
  bfa: {
    enabled: true,
    resolve: "./src/plugins/bfa",
    config: {
      salaryEnabled: env.bool("BFA_SALARY_ENABLED", false),
      scanLimit: env.int("BFA_SCAN_LIMIT", 1),
      apiKey: env("BFA_API_KEY", "infosysbub-ega"),
      apiUrl: env(
        "BFA_API_URL",
        "https://rest.arbeitsagentur.de/infosysbub/entgeltatlas/pc/v1/entgelte/",
      ),
    },
  },
  "field-uuid": {
    enabled: true,
    resolve: "./src/plugins/auto-uuid",
  },
  "audit-log": {
    enabled: true,
    resolve: "./src/plugins/audit-log",
    config: {
      deletion: {
        enabled: true,
        frequency: "logAge", // "logAge" or "logCount"
        options: {
          value: 2,
          interval: "week", // "day" or "week" or "month" or "year" // Don't add this config property if the frequency is "logCount"
        },
      },
      filters: {
        endpoint: {
          exclude: [
            "/content-manager/uid",
            "/admin/renew-token",
            "/admin/init",
            "/_health",
          ],
        },
        status: {},
        method: {
          exclude: ["GET", "OPTIONS"],
        },
      },
      redactedValues: [
        "password",
        "token",
        "firstname",
        "lastname",
        "username",
      ],
    },
  },
  anonymize: {
    enabled: env.bool("ANONYMIZE_ENABLED", false),
    resolve: "./src/plugins/anonymize",
    config: {
      cron: env("ANONYMIZE_CRON", "*/10 * * * * *"),
      enabled: env.bool("ANONYMIZE_ENABLED", false),
      anonymize: [
        {
          collection: "plugin::bfa.bfa-beruf",
          filters: {
            bkz: {
              $contains: "B 11282-103", //"B 11282-103", // single value. Usefull, if you want to anonymize one Record.
            },
          },
          limit: env.int("ANONYMIZE_LIMIT", 4),
          keepData: ["bkz", "zustand", "test_anonymize_at_date"],
          rules: {
            // Truncate string to first 3 characters
            suchnameNeutral: { type: "truncate", length: 3 }, // e.g., "ABCDEFG" → "ABC"

            // Replace value with a fixed string
            lbkgruppe: { type: "replace", value: "REDACTED" }, // e.g., "Original Value" → "REDACTED"
            type: { type: "replace", value: "t" }, // e.g., "Original Value" → "t"

            // Prefix string with specified text
            bezeichnungNeutral: { type: "prefix", prefix: "anon-" }, // e.g., "Name" → "anon-Name"

            // Set fixed date values (can use one or all of year, month, day)
            //** won't be applied as createdAt field can't be changed. but you can use this rule to any date field like this **//
            test_date: { type: "date", year: 2000, month: 1, day: 1 }, // e.g., "2023-08-10" → "2000-01-01"

            // Mask with * for the first 4 characters
            bezeichnungKurzNeutral: { type: "mask", length: 4 }, // e.g., "Visible" → "****ible"

            // Hash the string value
            test_hash: { type: "hash" }, // e.g., "SensitiveInfo" → "6b1b36cbb04b41490bfc0ab2bfa26f86" (SHA-256)

            // Randomize a numeric field within a specified range
            ebene: { type: "randomize", dataType: "number", min: 1, max: 100 }, // e.g., 23 → (random number between 1 and 100)

            // Randomize a string field to a specific length
            test_randomize: {
              type: "randomize",
              dataType: "string",
              length: 5,
            }, // e.g., "ABCDEF" → "x3a5z" (random 5 characters)   *** bkz is unique, if you want to test this rule remove unique from bkz in schema ***
            jsonField: {
              type: "json",
              fieldType: "date",
              dateRule: {
                year: 2000,
                month: 1,
                day: 1,
              },
            },
            // This rule processes JSON fields recursively.
            // For objects, it looks for keys that match the specified `fieldType` (e.g., 'date') and applies the `dateRule`.
            // For example:
            // Input: { "answer": "2023-08-10", "question": "What is your date of birth?" }
            // Output: { "answer": "2000-01-01", "question": "What is your date of birth?" }
            // For arrays, each item is processed similarly, applying the same anonymization logic.
          },
          delay: {
            field: "zustand",
            start: "test_anonymize_at_date", // References the date field in the schema
            rules: {
              // If zustand = "A", anonymize after a short delay
              A: {
                days: 0,
                hours: 0,
                minutes: 1, // Anonymize after 1 minute
                seconds: 0,
              },
              // If zustand = "E", anonymize after a longer delay
              E: {
                days: 10, // Anonymize after 1 day and
                hours: 12, // 12 hours
              },
            },
          },
        },
        {
          collection: "plugin::bfa.bfa-kategorie",
          filters: {
            value: 1,
          },
          limit: env.int("ANONYMIZE_LIMIT", 4),
          keepData: ["field"],
        },
        {
          collection: "api::article.article",
          filters: {
            title: "my title",
          },
          limit: env.int("ANONYMIZE_LIMIT", 4),
          keepData: ["title", "bfa_berufe"],
          rules: {
            description: { type: "replace", value: "REDACTED" },
          },
        },
      ],
    },
  },
  history: {
    enabled: true,
    resolve: "./src/plugins/history",
    config: {
      collections: {
        articles: ["title", "description", "bfa_berufe"], // test collection
        bfa_beruf: ["bkz", "ebene"],
      },
    },
  },
});
