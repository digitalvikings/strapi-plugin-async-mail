import { Strapi } from "@strapi/strapi";
import { Config, ContentType } from "../types";
import _ from "lodash";

export default async (strapi: Strapi) => {
  const config: Config = await strapi.config.get("plugin.history");

  const { collections } = config;

  const getContentTypes = async () => {
    const contentTypes: (ContentType | null)[] = Object.values(strapi.contentTypes)
      .filter((contentType) =>
        (contentType.kind === "collectionType" || contentType.kind === "singleType")
      )
      .map((contentType) => {
        const { uid, collectionName, globalId, apiName } = contentType;
        const collection = collections && Object.keys(collections).find((key) => {
          return (
            key === uid ||
            key === collectionName ||
            key === globalId ||
            key === apiName
          );
        });
        return collection ? { collection, uid } : null;
      })
      .filter((uid) => uid !== null);

    return contentTypes;
  };

  const extractChangedFields = (previous, current, updatedAt, user) => {
    const changes = {
      updatedAt,
      user,
      fields: {},
    };

    for (const key in current) {
      if (Object.prototype.hasOwnProperty.call(current, key)) {
        if (previous[key] !== current[key]) {
          if (current[key].connect) {
            if ((current[key].connect?.length || current[key].disconnect?.length))
              changes.fields[key] = { previous: previous[key], current: current[key] };
          } else {
            changes.fields[key] = { previous: previous[key], current: current[key] };
          }
        }
      }
    }

    return changes.fields && Object.keys(changes.fields).length > 0 ? changes : null;
  };


  function extractHistoryField(attributes) {
    for (const [key, value] of Object.entries(attributes)) {
      if (
        value &&
        typeof value === 'object' &&
        'customField' in value &&
        (value as any).customField === 'plugin::history.History'
      ) {
        return key;
      }
    }
    return null;
  }

  const contentTypes = await getContentTypes();
  if (!contentTypes || contentTypes.length === 0) return;

  strapi?.db?.lifecycles.subscribe({
    models: contentTypes.map((ct) => ct?.uid).filter((uid): uid is string => uid !== undefined),

    async afterFindMany(event) {
      // @ts-ignore
      const { model, params, result } = event;
      const { populate } = params;
      const { attributes } = model;

      const historyField = extractHistoryField(attributes);

      // Normalize populate into an array for consistency
      let populateArray: any = [];
      if (Array.isArray(populate)) {
        populateArray = populate;
      } else if (typeof populate === 'object' && populate !== null) {
        populateArray = Object.keys(populate);
      }

      if (historyField && populateArray.includes(historyField)) {
        for (const item of result) {
          const history = await strapi?.db?.query('plugin::history.history').findOne({
            where: {
              entityId: item.id,
              collectionName: model.uid,
            },
            select: ['collectionName', 'fields', 'updates', 'entityId'],
          });
          item[historyField] = history || null;
        }
      } else {
        for (const item of result) {
          delete item[historyField];
        }
      }
    },


    async afterFindOne(event) {
      // @ts-ignore
      const { model, params, result } = event;
      const { populate } = params;
      const { attributes } = event.model;
      const historyField = extractHistoryField(attributes)

      let populateArray: any = [];
      if (Array.isArray(populate)) {
        populateArray = populate;
      } else if (typeof populate === 'object' && populate !== null) {
        populateArray = Object.keys(populate);
      }

      if (historyField && populateArray.includes(historyField)) {
        const history = await strapi?.db?.query('plugin::history.history').findOne({
          where: {
            entityId: result.id,
            collectionName: model.uid,
          },
          select: ['collectionName', 'fields', 'updates', 'entityId']
        });
        result[historyField] = history || null;
      } else {
        delete result[historyField]
      }

    },

    async beforeUpdate(event) {
      const { model, params } = event;
      const { data, where } = params;
      const id = where.id
      const ctx = strapi.requestContext.get();

      if (!ctx) return
      const userType = ctx.request.url.includes('/content-manager/') ? 'Admin User' : 'API User'

      const userData = ctx.state.user

      const user = userData ? {
        id: userData?.id,
        firstname: userData?.firstname,
        lastname: userData?.lastname,
        username: userData?.username,
        email: userData?.email,
        userType
      } : {
        userType: 'public'
      }

      // Ensure there's a previous state
      const previous = id
        ? await strapi?.db?.query(model.uid).findOne({
          where: { id },
        })
        : null;

      if (!previous) return;

      // Extract changed fields
      const contentType = contentTypes.find(ele => ele?.uid === model.uid);
      const fields =
        collections &&
          contentType &&
          contentType.collection
          ? collections[contentType.collection]
          : undefined;

      const changes = fields
        ? extractChangedFields(
          _.pick(previous, fields),
          _.pick(data, fields),
          data.updatedAt,
          user
        )
        : null;
      if (changes) {
        // Call the updateHistory service
        await strapi
          .plugin("history")
          .service("history")
          .updateHistory({
            collectionName: model.uid,
            entityId: `${id}`,
            changes,
            fields,
          });
      }
    }

  });
};
