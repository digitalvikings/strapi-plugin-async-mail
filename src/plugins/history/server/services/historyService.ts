import { Strapi } from "@strapi/strapi";

export default ({ strapi }: { strapi: Strapi }) => ({
  async updateHistory(data) {
    const { collectionName, entityId, changes, fields } = data;

    // Find an existing history log
    const historyLog = await strapi?.db?.query("plugin::history.history").findOne({
      where: {
        collectionName,
        entityId,
      },
    });

    if (!historyLog) {
      // If no history log exists, create a new one
      await strapi?.entityService?.create("plugin::history.history", {
        data: {
          collectionName,
          entityId,
          fields,
          updates: [changes],
        },
      });
    } else {
      // Append the changes to the existing history log
      const history = historyLog.updates || []
      const updatedChanges = [...history, changes];
      await strapi?.db?.query("plugin::history.history").update({
        where: { id: historyLog.id },
        data: {
          updates: updatedChanges,
          fields
        },
      });
    }
  },
});
