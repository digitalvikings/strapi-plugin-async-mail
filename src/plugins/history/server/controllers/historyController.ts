import { Strapi } from '@strapi/strapi';

export default ({ strapi }: { strapi: Strapi }) => ({
  async getHistory(ctx) {
    try {
      const {
        params: { collectionName, entityId },
      } = ctx.request;
      const result = await strapi.db
        .query('plugin::history.history')
        .findOne({
          where: { collectionName, entityId },
        });
      return result;
    } catch (error) {
      ctx.throw(500, error);
    }
  },
});
