export default [
  {
    method: "GET",
    path: "/:collectionName/:entityId",
    handler: "historyController.getHistory",
    config: {
      // policies: ['admin::isAuthenticatedAdmin'],
      // auth: false, // remove for restricted public access e.g. /history/api::article.article/1
    },
  },
];
