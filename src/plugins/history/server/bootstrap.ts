import { Strapi } from '@strapi/strapi';
import historyLogs from "./boot/historyLogs";

export default ({ strapi }: { strapi: Strapi }) => {
  // bootstrap phase
  historyLogs(strapi)
};
