import { Strapi } from '@strapi/strapi';

export default ({ strapi }: { strapi: Strapi }) => {
  // register phase
  strapi.customFields.register({
    name: "History",
    plugin: "history",
    type: "json",
    // inputSize: {
    //   // optional
    //   default: 4,
    //   isResizable: true,
    // },
  });
};
