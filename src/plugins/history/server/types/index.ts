export type Config = {
  collections?: Record<string, string[]>;
};

export type ContentType = {
  collection?: string;
  uid?: string;
};


export type FieldDetails = {
  type: string;
  target: string[];
}

export type EventModel = {
  attributes: Record<string, FieldDetails>;
}

