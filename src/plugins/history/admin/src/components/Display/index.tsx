import React, { useState, useEffect, useCallback } from "react";
import {
  Box,
  Typography,
  Select,
  Option,
  Stack,
  Divider,
  Flex,
  Button,
  IconButton,
} from "@strapi/design-system";
import { request } from "@strapi/helper-plugin";
import { useParams } from "react-router-dom";
import { Refresh } from "@strapi/icons";

import HistoryItem from "../HistoryItem";
import "./index.css";
import _ from "lodash";

const HistoryDisplay = () => {
  const [historyData, setHistoryData] = useState<any>(null);
  const [selectedField, setSelectedField] = useState<any>(null);
  const [loading, setLoading] = useState(true);
  const [isCollapsed, setIsCollapsed] = useState(false); // Collapsible state
  const { slug, id } = useParams<{ slug: string; id: string }>();

  const fetchHistory = useCallback(async () => {
    try {
      setLoading(true);
      const res = await request(`/history/${slug}/${id}`, {
        method: "GET",
      });

      if (res) {
        setHistoryData(res);

        // Check if the current selectedField is still valid
        if (!res.fields.includes(selectedField)) {
          setSelectedField(res.fields[0]); // Default to the first field if the current one is invalid
        }
      }
    } catch (error) {
      console.error("Failed to fetch history data:", error);
    } finally {
      setLoading(false);
    }
  }, [slug, id, selectedField]);

  useEffect(() => {
    const submitButton: any = document.querySelector('button[type="submit"]');

    const handleClick = () => {
      setTimeout(() => {
        fetchHistory();
      }, 500);
    };

    if (submitButton) {
      submitButton.addEventListener("click", handleClick);
    }

    // Cleanup the listener
    return () => {
      if (submitButton) {
        submitButton.removeEventListener("click", handleClick);
      }
    };
  }, [fetchHistory]);

  useEffect(() => {
    fetchHistory();
  }, [fetchHistory]);
  if (loading) {
    return <Typography variant="omega">Loading history...</Typography>;
  }

  if (!historyData || !historyData.updates?.length) {
    return (
      <Box>
        <Typography variant="omega" fontWeight="bold">
          History
        </Typography>
        <Divider style={{ margin: "1rem 0" }} />
        <Typography variant="omega" textColor="neutral600">
          No history data available.
        </Typography>
      </Box>
    );
  }

  return (
    <Box>
      <Flex>
        <Typography variant="omega" fontWeight="bold">
          History
        </Typography>
        <IconButton
          style={{ marginLeft: "20px" }}
          label="Reload history"
          icon={<Refresh />}
          // variant="secondary"
          onClick={fetchHistory}
        />
      </Flex>
      <Divider style={{ margin: "1rem 0" }} />

      <Box marginBottom={4} style={{ width: "25%" }}>
        <Typography variant="pi" fontWeight="semiBold" textColor="neutral800">
          Field:
        </Typography>
        <Select
          id="field-select"
          value={selectedField}
          onChange={(value: React.SetStateAction<string | null>) => {
            setSelectedField(value);
          }}
        >
          {historyData.fields.map((field: string, idx: string) => (
            <Option key={`field-${idx}`} value={field}>
              {field}
            </Option>
          ))}
        </Select>
      </Box>

      {_.compact(_.map(historyData.updates, selectedField))?.length > 0 && (
        <Button
          onClick={() => setIsCollapsed(!isCollapsed)}
          variant="secondary"
          style={{ marginBottom: "1rem" }}
        >
          {isCollapsed ? "Show History" : "Hide History"}
        </Button>
      )}

      {!isCollapsed &&
      _.map(
        _.filter(historyData.updates, (update) =>
          _.has(update.fields, selectedField),
        ),
        (update: any) => ({
          updatedAt: update.updatedAt,
          user: update.user,
          change: update.fields[selectedField],
        }),
      ).length ? (
        <Stack spacing={4} className="history-scroll">
          <Flex direction="column" className="history-flex">
            {_.map(
              _.filter(historyData.updates, (update) =>
                _.has(update.fields, selectedField),
              ),
              (update: any) => ({
                updatedAt: update.updatedAt,
                user: update.user,
                change: update.fields[selectedField],
              }),
            ).map((item, idx: any) => (
              <Box
                hasRadius={true}
                shadow="popupShadow"
                padding={6}
                background="neutral0"
                className="history-box"
                marginBottom={8}
                key={`box-${idx}`}
                style={{ width: "100%", marginRight: "20px" }}
              >
                <HistoryItem item={item} key={`historyItem-${idx}`} />
              </Box>
            ))}
          </Flex>
        </Stack>
      ) : (
        isCollapsed || (
          <Typography variant="omega" textColor="neutral600">
            No updates available for {selectedField} field.
          </Typography>
        )
      )}
    </Box>
  );
};

export default HistoryDisplay;
