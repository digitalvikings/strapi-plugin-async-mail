import React from "react";
import {
  Box,
  Grid,
  GridItem,
  Typography,
  Divider,
} from "@strapi/design-system";
import { ArrowRight } from "@strapi/icons";
const moment = require("moment");

export default function HistoryItem({
  item,
}: {
  item: { updatedAt: string; user: any; change: any };
}) {
  const { change, user, updatedAt } = item;

  const date = moment(updatedAt).format("DD/MM/YYYY");
  const time = moment(updatedAt).format("HH:mm:ss");

  const display: any = {};
  if (change.current.connect) {
    display.relation = true;
    display.current = `+ ${change.current.connect.map((ele: any) => "id: " + ele.id).join()}`;
    display.previous = `- ${change.current.disconnect.map((ele: any) => "id: " + ele.id).join()}`;
  } else {
    display.relation = false;
    display.current = change.current;
    display.previous = change.previous;
  }

  return (
    <Grid>
      <GridItem col={9}>
        <Box marginBottom={2}>
          <Box padding={2}>
            <Typography variant="omega">
              {
                <span
                  className={`history-status ${display.relation && "previous"}`}
                >{`${display.previous}`}</span>
              }
              &nbsp;&nbsp; &nbsp;
            </Typography>
            <ArrowRight style={{ marginBottom: "-5px" }} /> &nbsp;&nbsp;{" "}
            <Typography variant="omega">
              {
                <span
                  className={`history-status ${display.relation && "current"}`}
                >{`${display.current}`}</span>
              }
            </Typography>
          </Box>
        </Box>
      </GridItem>
      <GridItem col={3} style={{ marginLeft: "70px" }}>
        <Box marginBottom={2}>
          <Typography variant="omega" textColor="neutral500">
            Date:
          </Typography>
          <Typography variant="omega"> {date}</Typography>
        </Box>
        <Box>
          <Typography variant="omega" textColor="neutral500">
            Time:
          </Typography>
          <Typography variant="omega"> {time}</Typography>
        </Box>
      </GridItem>

      {user && (
        <GridItem col={12}>
          <Divider unsetMargin={false} />
        </GridItem>
      )}
      {user && (
        <GridItem col={12} className="owner">
          <Typography variant="pi" textColor="neutral500">
            {" "}
            Updated by:&nbsp; &nbsp;
          </Typography>
          {user.userType == "public" ? (
            <Typography variant="pi" fontWeight="bold">
              Anonymous User
            </Typography>
          ) : (
            <Typography variant="pi" fontWeight="bold">
              {user.firstname || user.username}
              {user.lastname ? ` ${user.lastname}` : ""}
            </Typography>
          )}
          <Typography variant="pi" textColor="neutral500">
            &nbsp; &nbsp; ( {user.userType} )
          </Typography>
        </GridItem>
      )}
    </Grid>
  );
}
