# Strapi plugin bfa

https://www.arbeitsagentur.de/datei/Klassifikation-der-Berufe_ba017989.pdf

## Configuration:

The default configuration is as follows (you must add it to `./config/plugin.js`):

```js
module.exports = {
  // ...
  bfa: {
    enabled: true,
    config: {
      salaryEnabled: env.bool("BFA_SALARY_ENABLED", false),
      scanLimit: env.int("SCAN_LIMIT", 1),
      apiKey: env("BFA_API_KEY", "infosysbub-ega"),
      apiUrl: env(
        "BFA_API_URL",
        "https://rest.arbeitsagentur.de/infosysbub/entgeltatlas/pc/v1/entgelte/"
      ),
    },
  },
  // ...
}
```



## License

This Projet is licensed under the MIT License. See the LICENSE file for more information.


