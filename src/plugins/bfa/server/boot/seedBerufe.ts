const berufe = require('../content-types/bfa-berufe/vam_beruf_kurz.json');
const UID = "plugin::bfa.bfa-beruf";
const BATCH_SIZE = 500; // Adjust this size depending on your database limits

export default async (strapi) => {
  strapi.log.debug("Create bfa-beruf collection");

  // Fetch all existing IDs once
  const existingIds = (await strapi.db.query(UID).findMany({
    select: ['id'],
  })).map(item => item.id);

  // Filter to get only new entries
  const newEntries = berufe.filter(beruf => !existingIds.includes(beruf.id));

  // Helper function to chunk an array into smaller arrays of a given size
  const chunkArray = (array, size) =>
    array.reduce((acc, _, i) => 
      (i % size ? acc : [...acc, array.slice(i, i + size)]), []);

  // Batch insert in chunks
  const chunks = chunkArray(newEntries, BATCH_SIZE);

  for (const chunk of chunks) {
    try {
      await strapi.db.query(UID).createMany({
        data: chunk,
      });
      // strapi.log.info(`Inserted ${chunk.length} new berufe entries`);
    } catch (error) {
      strapi.log.error("Error inserting berufe entries in batch:", error);
    }
  }
};
