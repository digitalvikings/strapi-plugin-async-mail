const regionen = require('../content-types/bfa-regionen/regionen.json')

const UID = "plugin::bfa.bfa-region";

export default async (strapi) => {
  strapi.log.debug("Create bfa-region collection");
  for (const region of regionen) {
    const entity = await strapi.db.query(UID).findOne({
      where: {
        vam: region.vam
      },
    });

    if (!entity) {
      try {
        await strapi.db.query(UID).create({
          data: region,
        });
      } catch (error) {
        console.log(error, entity);
      }
    }
  }
};
