const kategorien = require('../content-types/bfa-categories/categories.json')

const UID = "plugin::bfa.bfa-kategorie";

export default async (strapi) => {
  strapi.log.debug("Create bfa-kategorie collection");
  for (const kategorie of kategorien) {
    const entity = await strapi.db.query(UID).findOne({
      where: {
        field: kategorie.field,
        label: kategorie.label,
      },
    });

    if (!entity) {
      try {
        await strapi.db.query(UID).create({
          data: kategorie,
        });
      } catch (error) {
        console.log(error, entity);
      }
    }
  }
};
