import berufe from "./bfa-berufe";
import kategorie from "./bfa-categories";
import regionen from "./bfa-regionen";
import salary from "./bfa-salary";

export default {
  "bfa-beruf": berufe,
  "bfa-kategorie": kategorie,
  "bfa-region": regionen,
  "bfa-salary": salary
 };

