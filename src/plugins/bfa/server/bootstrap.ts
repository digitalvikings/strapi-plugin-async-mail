'use strict';

import { Strapi } from '@strapi/strapi';
import { Config } from "./types";
import seedBerufe from "./boot/seedBerufe";
import seedKategorien from "./boot/seedKategorien";
import seedRegionen from "./boot/seedRegionen";

export default ({ strapi }: { strapi: Strapi }) => {
  seedKategorien(strapi);
  seedBerufe(strapi);
  seedRegionen(strapi);

  const config: Config = strapi.config.get("plugin.bfa");
  const { salaryEnabled } = config;

  if (!salaryEnabled) return

  strapi.cron.add({
    updateSalary: {
      task: async ({ strapi }) => {
        const service = strapi.plugin("bfa").service("updateSalary");
        try {
          const jobs = await service.scan();
          for (const job of jobs) {
            await service.updateSalary(job);
          }
          console.log("Check Salary", service)
        } catch (error) {
          strapi.log.error("Failed to send emails:", error);
        }
      },
      options: "*/2 * * * *",
    },
  });
};
