import service from "./bfa";
import berufe from "./bfa-berufe";
import updateSalary from "./update-salary";

export default {
  bfa: service,
  updateSalary: updateSalary,
  berufe,
};
