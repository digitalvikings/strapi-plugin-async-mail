import { Strapi } from "@strapi/strapi";
import utils from "@strapi/utils";

const { errors } = utils;

export default ({ strapi }: { strapi: Strapi }) => ({
  async find(query = {}) {
    try {
      return await strapi.plugin("bfa").service("berufe").find(query);
    } catch (error) {
      strapi.log.error(error);
      throw new errors.ApplicationError(error.message);
    }
  },
});
