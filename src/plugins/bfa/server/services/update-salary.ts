import { Strapi } from "@strapi/strapi";
import utils from "@strapi/utils";
import { Config, Job } from "../types";
import axios from 'axios';
import _ from "lodash";

const { errors } = utils;

const UID = "plugin::bfa.bfa-salary";

export default ({ strapi }: { strapi: Strapi }) => ({
  async find(query = {}) {
    try {
      console.log("Find Jobs without Salary", query)
      return await strapi.plugin("bfa").service("berufe").find(query);
    } catch (error) {
      console.log("error")
      strapi.log.error(error);
      throw new errors.ApplicationError(error.message);
    }
  },

  async scan(filters = {}) {
    const config: Config = await strapi.config.get("plugin.bfa");
    const { scanLimit } = config;
    try {
      return await await strapi.entityService.findMany(
        "plugin::bfa.bfa-beruf",
        {
          filters: {
            salary: null,
          },
          limit: scanLimit || 10,
        }
      );
    } catch (error) {
      strapi.log.error("Failed to scan emails:", error);
      throw new errors.ApplicationError("Failed to scan jobs");
    }
  },

  async saveData(id, data = {}) {
    try {
      const salary = await strapi.entityService?.create(UID, {
        data: {
          kldb2010: id,
          salary: data
        }});

      console.log("SAVE", id, data, salary);
      const berufe = await strapi.db.query('plugin::bfa.bfa-beruf').findMany({
        where: {
          bkz: {
            $contains: id.toString()
          },
        }
      })

      const links = berufe.map(code => ({ bfa_beruf_id: code.id, bfa_salary_id: salary.id }))

      await strapi.db.connection.insert(links).into('bfa_beruf_salary_links')

    } catch (error) {
      console.log(error)
    }
  },

  async updateSalary(job: Job) {
    const config: Config = await strapi.config.get("plugin.bfa");
    const { apiUrl, apiKey } = config;
    try {
      const jobId = job.id
      const bkz = job.bkz
      const re = /B (\d+)-/;
      const found = bkz.match(re);
      const complexityLevel = found[1].toString().slice(-1);

      const url = `${apiUrl}${found[1]}`;
      console.log("JOB", bkz, complexityLevel, found, url)
      return await axios.get(url,
        {
          headers: {
            "X-Api-Key": apiKey,
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36"
          },
          params: {
            l: complexityLevel,
            r: "1",  // region
            a: "1",  // age
            b: "1",  // branche
          },
        }
      )
      .then((response) => {
        this.saveData(found[1], response.data);
      })
      .catch((error) => {
        console.error(error);
      });

    } catch (error) {
      strapi.log.error("Failed to scan emails:", error);
      throw new errors.ApplicationError("Failed to scan jobs");
    }
  }
});
