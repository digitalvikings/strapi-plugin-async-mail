export type Config = {
  apiKey?: string;
  apiUrl?: string;
  scanLimit?: number;
  salaryEnabled: boolean;
};

export type Job = {
  id: number;
  bkz: string;
  name?: string;
};
