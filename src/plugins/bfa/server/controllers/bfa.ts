import { Strapi } from "@strapi/strapi";

export default ({ strapi }: { strapi: Strapi }) => ({
  async find(ctx) {
    try {
      ctx.body = await strapi
        .plugin("bfa")
        .service("bfa")
        .find(ctx.request.query);
    } catch (error) {
      throw error;
    }
  },
});
