# Strapi plugin anonymize

the plugin filters data from collections and anonymizes all fields that do not appear in a whitelist. 
It expects a boolean field “anonymized” in the collection that is to be anonymized.

Rules can be defined for the anonymization of individual fields. 

A delay depending on the value of a field can be defined to allow a period of time to elapse between an
approval for anonymization and the actual anonymization.

## Status

WIP - Work in Progress/Planning

## Install

yarn add @cross-solution/strapi-plugin-anonymize

## Configuration:

The default configuration is as follows (you must add it to `./config/plugin.js`):

```js
module.exports = {
  // ...
  "anonymize": {
    cron: "*/10 * * * * *",
    enabled: true,
    config: {
      anonymize: [
        {
          collection: "Strapi UID, eg. plugin::bfa.bfa-beruf",
          filters: {
            foo: {
              $contains: "bar",
            },
          },
          limit: 4,
          keepData: ['field1', 'field2'],
          rules: {
            // Truncate string to first 3 characters
            field1: { type: "truncate", length: 3 }, // e.g., "ABCDEFG" → "ABC"

            // Replace value with a fixed string
            field3: { type: "replace", value: "REDACTED" }, // e.g., "Original Value" → "REDACTED"
            field4: { type: "replace", value: "t" }, // e.g., "Original Value" → "t"

            // Prefix string with specified text
            field5: { type: "prefix", prefix: "anon-" }, // e.g., "Name" → "anon-Name"

            // Set fixed date values (can use one or all of year, month, day)
            //** won't be applied as createdAt field can't be changed. but you can use this rule to any date field like this **//
            field6: { type: "date", year: 2000, month: 1, day: 1 }, // e.g., "2023-08-10" → "2000-01-01"

            // Mask with * for the first 4 characters
            field7: { type: "mask", length: 4 }, // e.g., "Visible" → "****ible"

            // Hash the string value
            field8: { type: "hash" }, // e.g., "SensitiveInfo" → "6b1b36cbb04b41490bfc0ab2bfa26f86" (SHA-256)

            // Randomize a numeric field within a specified range
            field9: { type: "randomize", dataType: "number", min: 1, max: 100 }, // e.g., 23 → (random number between 1 and 100)

            // Randomize a string field to a specific length
            field9: {
              type: "randomize",
              dataType: "string",
              length: 5,
            }, // e.g., "ABCDEF" → "x3a5z" (random 5 characters)   *** bkz is unique, if you want to test this rule remove unique from bkz in schema ***
          },
          delay: {
            field: "state",
            rules: {
              // If state = "A", anonymize after a short delay
              "A": { 
                days: 0,
                hours: 0,
                minutes: 1,  // Anonymize after 1 minute
                seconds: 0
              },
              // If state = "E", anonymize after a longer delay
              "E": { 
                days: 1,     // Anonymize after 1 day and
                hours: 12,   // 12 hours 
              }
            }
          }
        },
      ]
    }
  }
  // ...
}
```

## Anonymization Rules

Truncate: Limits the length of a string to a specified number of characters.
`{ type: "truncate", length: 2 }, // "60486" -> "60"`

Replace: Replaces the entire value with a predefined value.
`{ type: "replace", value: "REDACTED" }, // "John Doe" -> "REDACTED"`

Prefix: Adds a prefix to the beginning of a string.
`{ type: "prefix", prefix: "ANON-" }, // "12345" -> "ANON-12345"`


Date: Modifies only parts of a date while preserving the rest.
`{ type: "date", year: 2000, month: 1, day: 1 }, // "2023-08-20" -> "2000-01-01"`

Mask: Hides part of the string with asterisks, leaving a specific number of characters visible.
`{ type: "mask", length: 3 }, // "12345678" -> "***45678"`

Hash: Hashes a string for anonymization.
`{ type: "hash" }, // "secret" -> "2bb80d... (hashed)"`

Randomize: Generates a random value within a defined range.

```
{ type: "randomize", dataType: "number", min: 1000, max: 9999 }, // e.g., 1234
{ type: "randomize", dataType: "string", length: 6 }, // e.g., "abc123"
```
