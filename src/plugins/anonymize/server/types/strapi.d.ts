import '@strapi/database';

declare module '@strapi/database' {
  interface QueryOptions {
    anonymize?: boolean;
  }
} 