import { UID } from '@strapi/types'

export type Config = {
  anonymize: Anonymize[];
  cron: string;
  enabled: boolean;
};

export type Anonymize = {
  collection: UID.ContentType,
  filters: object,
  rules: object,
  delay: DelayConfig
  limit: number
  keepData: string[]
}

export interface DelayRule {
  days?: number;
  hours?: number;
  minutes?: number;
  seconds?: number;
}

export interface DelayConfig {
  start?: string;
  field: string;
  rules: {
    [key: string]: DelayRule;
  };
}


