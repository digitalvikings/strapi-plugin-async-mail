export default {
  default: {
    anonymize: [
      {
        collection: null,
        filters: {},
        limit: 2
      }
    ],
    cron: "*/5 * * * *",
    enabled: false
  },
  validator() {

  },
};
