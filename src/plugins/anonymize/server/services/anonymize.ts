import { Strapi } from "@strapi/strapi";
import utils from "@strapi/utils";
import { Anonymize, DelayConfig } from "../types";
import _ from "lodash";
import moment from 'moment'
const { errors } = utils;


// Utility function to apply anonymization rules
const applyRule = (value, rule) => {

  if (value === undefined || value === null) {
    return value;
  }

  switch (rule.type) {
    case "truncate":
      return typeof value === "string" ? value.slice(0, rule.length) : value;

    case "replace":
      return rule.value;

    case "prefix":
      return typeof value === "string" ? rule.prefix + value : value;

    case "date":
      if (value instanceof Date || !isNaN(Date.parse(value))) {
        const date = new Date(value);
        if (rule.year) date.setFullYear(rule.year);
        if (rule.month) date.setMonth(rule.month - 1);
        if (rule.day) date.setDate(rule.day);
        return date; // .toISOString().split('T')[0];
      }
      return value;

    case "mask":
      const maskLength = rule.length || value.length;
      return typeof value === "string" ? "*".repeat(maskLength) + value.slice(maskLength) : value;

    case "hash":
      return typeof value === "string" ? require('crypto').createHash('sha256').update(value).digest('hex') : value;

    case "randomize":
      if (rule.dataType === "number") {
        return Math.floor(Math.random() * (rule.max - rule.min) + rule.min);
      }
      if (rule.dataType === "string") {
        return Math.random().toString(36).substring(2, rule.length + 2);
      }
      return value;

    case "json":
      if (typeof value === "object" && !Array.isArray(value)) {
        // Process JSON object

        return Object.entries(value).reduce((acc, [key, fieldValue]) => {

          const condition = value.type === 'date' && key === "answer" && rule.fieldType === "date" && typeof fieldValue === "string" && !isNaN(Date.parse(fieldValue))

          if (condition) {

            acc[key] = moment(applyRule(fieldValue, { type: "date", ...rule.dateRule })).format('DD.MM.YYYY');

          } else {
            acc[key] = fieldValue;
          }
          return acc;
        }, {});
      } else if (Array.isArray(value)) {
        // Process JSON array
        return value.map(item => applyRule(item, rule));
      }
      return value;

    default:
      return null;
  }
};


/*  Anonymization Rules

   Truncate: Limits the length of a string to a specified number of characters.
   { type: "truncate", length: 2 }, // "60486" -> "60"
----------------------------------------------------------------------------------

   Replace: Replaces the entire value with a predefined value.
   { type: "replace", value: "REDACTED" }, // "John Doe" -> "REDACTED"

----------------------------------------------------------------------------------

   Prefix: Adds a prefix to the beginning of a string.
   { type: "prefix", prefix: "ANON-" }, // "12345" -> "ANON-12345"

----------------------------------------------------------------------------------

   Date: Modifies only parts of a date while preserving the rest.
   { type: "date", year: 2000, month: 1, day: 1 }, // "2023-08-20" -> "2000-01-01"

----------------------------------------------------------------------------------

   Mask: Hides part of the string with asterisks, leaving a specific number of characters visible.
   { type: "mask", length: 3 }, // "12345678" -> "***45678"

----------------------------------------------------------------------------------

   Hash: Hashes a string for anonymization.
   { type: "hash" }, // "secret" -> "2bb80d... (hashed)"

----------------------------------------------------------------------------------

   Randomize: Generates a random value within a defined range.
   { type: "randomize", dataType: "number", min: 1000, max: 9999 }, // e.g., 1234
   { type: "randomize", dataType: "string", length: 6 }, // e.g., "abc123"

----------------------------------------------------------------------------------
*/


/**
 * Checks if the configured delay period has elapsed for a record
 * @param record - The database record to check
 * @param delay - Delay configuration object containing field and rules
 * @returns boolean - True if the delay period has elapsed or no delay is configured
 */
const hasDelayElapsed = (record, delay: DelayConfig): boolean => {
  // Early return if required configuration is missing
  if (!delay || !_.has(delay, 'field') || !_.has(delay, 'rules')) {
    return true;
  }

  const fieldValue = _.get(record, delay.field);
  const delayRule = _.get(delay.rules, fieldValue);

  if (_.isEmpty(delayRule)) {
    return true;
  }

  let startDate: Date | null = null;

  if (delay.start) {
    // Get the value from the specified start field
    const startFieldValue = _.get(record, delay.start);

    if (startFieldValue instanceof Date || !isNaN(Date.parse(startFieldValue))) {
      startDate = new Date(startFieldValue);
    } else {
      strapi.log.warn(
        `Field "${delay.start}" does not contain a valid date value. Falling back to updatedAt.`
      );
    }
  }

  // Fall back to updatedAt if no valid start date
  if (!startDate) {
    const updatedAt = _.get(record, 'updatedAt');
    if (!updatedAt) {
      return true;
    }
    startDate = new Date(updatedAt);
  }

  const now = new Date();

  const timeUnits = [
    { unit: 'days', multiplier: 24 * 60 * 60 * 1000 },
    { unit: 'hours', multiplier: 60 * 60 * 1000 },
    { unit: 'minutes', multiplier: 60 * 1000 },
    { unit: 'seconds', multiplier: 1000 }
  ];

  const totalDelayMs = _.sum(
    timeUnits
      .filter(({ unit }) => _.has(delayRule, unit))
      .map(({ unit, multiplier }) =>
        _.multiply(_.get(delayRule, unit, 0), multiplier)
      )
  );

  const elapsedMs = now.getTime() - startDate.getTime();

  if (process.env.NODE_ENV === 'development') {
    console.log({
      fieldValue,
      delayRule,
      startField: delay.start,
      startDate: startDate.toISOString(),
      configuredUnits: timeUnits
        .filter(({ unit }) => _.has(delayRule, unit))
        .map(({ unit }) => unit),
      totalDelayMs,
      elapsedMs,
      shouldAnonymize: elapsedMs >= totalDelayMs
    });
  }
  return elapsedMs >= totalDelayMs;
};


export default ({ strapi }: { strapi: Strapi }) => ({
  async scan(config: Anonymize) {
    const { collection, filters, keepData, limit, rules = {}, delay } = config;

    const ct = await strapi.plugin('content-manager').service("content-types").findContentType(collection);
    try {
      // Prepare the fields to be anonymized
      if (!ct) {
        throw new errors.ApplicationError(`Content type ${collection} not found`);
      }


      const fields = Object.keys(ct.attributes);
      let protectedFields = [...new Set([...keepData, "id", "createdAt", "updatedAt", "createdBy", "updatedBy", "anonymized"])];

      const anonymizedFields = fields.filter(field => !protectedFields.includes(field));
      strapi.log.verbose(`Fields to be anonymized: ${anonymizedFields.join(", ")}`);

      const specialFields = anonymizedFields.filter(field => {
        const fieldAttribute = ct.attributes[field];
        return fieldAttribute && (fieldAttribute.type === "relation" || fieldAttribute.type === "media");
      });

      // Step 3: Use a raw query to update the records in the database


      const data = await strapi?.db?.query(collection).findMany({
        where: {
          ...filters,
          $or: [
            {
              anonymized: {
                $eq: false
              }
            },
            {
              anonymized: {
                $null: true
              }

            }
          ]
        },
        populate: specialFields,
        limit: limit
      });

      if (!data?.length) {
        strapi.log.debug('No records found to anonymize');
        return;
      }

      const readyToAnonymize = data.filter(record => hasDelayElapsed(record, delay));

      if (!readyToAnonymize.length) {
        strapi.log.debug('No records ready for anonymization after delay check');
        return;
      }

      const anonymizedData = anonymizedFields.reduce((acc, field) => {
        const rule = rules[field];
        if (data && data.length > 0 && data[0][field] !== undefined) {
          acc[field] = rule ? applyRule(data[0][field], rule) : null;
        } else {
          acc[field] = null;
        }
        return acc;
      }, { anonymized: true });

      const ids = readyToAnonymize?.map((record) => record.id);

      if (ids?.length == 0) {
        return
      }

      await strapi?.db?.query(collection).updateMany({
        where: {
          id: {
            $in: ids
          }
        },
        data: anonymizedData,
        //@ts-ignore
        options: { anonymize: true }
      })


      // Anonymize special fields individually
      readyToAnonymize && await Promise.all(readyToAnonymize.map(async (record) => {
        const specialFieldData = {};

        // Handle each special field
        for (const field of specialFields) {
          const fieldAttribute = ct.attributes[field];
          // If it's a media field, delete the files first
          if (fieldAttribute.type === 'media') {
            const mediaItems = record[field];
            if (mediaItems) {
              // Handle both single and multiple media fields
              const items = Array.isArray(mediaItems) ? mediaItems : [mediaItems];
              for (const item of items) {
                if (item?.id) {
                  await strapi.plugins.upload.services.upload.remove(item);
                }
              }
            }
          }

          specialFieldData[field] = null;
        }

        await strapi?.db?.query(collection).update({
          where: { id: record.id },
          data: { ...specialFieldData, anonymized: true },
          //@ts-ignore
          options: { anonymize: true }
        });
      }));

      strapi.log.info(`Anonymized ${ids?.length} records in ${collection} collection`);

      // After successful anonymization, handle history records
      const historyConfig = await strapi.config.get("plugin.history") as { collections?: Record<string, unknown> };
      if (historyConfig?.collections?.[ct.collectionName]) {
        // Get all history records for the anonymized entities
        const historyRecords = await strapi?.db?.query('plugin::history.history').findMany({
          where: {
            collectionName: collection,
            entityId: {
              $in: ids.map(id => id.toString())
            }
          }
        });

        // Process each history record
        for (const record of historyRecords) {
          if (record.updates) {
            const updatedHistory = record.updates.map(update => {
              // Only keep fields that are in keepData
              const filteredFields = {};
              for (const [field, value] of Object.entries(update.fields)) {
                if (keepData.includes(field)) {
                  filteredFields[field] = value;
                }
              }
              return {
                ...update,
                fields: filteredFields
              };
            });

            // Update the history record with filtered data
            await strapi?.db?.query('plugin::history.history').update({
              where: { id: record.id },
              data: {
                updates: updatedHistory
              }
            });
          }
        }

        strapi.log.info(`Updated history records for ${ids?.length} anonymized records in ${collection} collection`);
      }

    } catch (error) {
      strapi.log.error("Failed to anonymize data directly:", error);
      throw new errors.ApplicationError("Failed to anonymize data directly");
    }
  },
});
