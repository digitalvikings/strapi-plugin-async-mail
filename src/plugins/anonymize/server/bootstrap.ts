import { Strapi } from '@strapi/strapi';
import { Config } from "./types";

export default ({ strapi }: { strapi: Strapi }) => {
  const config: Config = strapi.config.get("plugin.anonymize");
  strapi.cron.add({
    anonymize: {
      task: async ({ strapi }) => {
        const service = strapi.plugin("anonymize").service("anonymize");

        try {
          for (const anonymize of config.anonymize) {
            await service.scan(anonymize);
            strapi.log.verbose("aNonymize", anonymize, service)
          }

        } catch (error) {
          strapi.log.error("Failed to anonymize:", error);
        }
      },
      options: config.cron,
    },
  });
};
