# Strapi plugin async-mail

# Status: Work in Progress

The `async-mail` plugin makes it possible to send form data from anonymous users to a defined email address. Form data is stored in a collection and sent asyncronously by a cronjob. A typical use case is a contact form that is supposed to send entered data to a defined email address.

##  🖐 Requirements

- strapi 4 
- nodes 18|20

## ⏳ Install

The `async-mail` is based on the Strapi Email Service. To send emails you need an email plugin. If you just want to send mails via SMTP, we recommend [@strapi/provider-email-nodemailer](https://github.com/strapi/strapi/tree/main/packages/providers/email-nodemailer). However, you can use any other [Email Provider Plugin](https://docs.strapi.io/cloud/advanced/email). The configuration of the email plugin provider used can be found in the corresponding documentation. E.g. Nodemailer configuration is located in the [Readme](https://github.com/strapi/strapi/tree/main/packages/providers/email-nodemailer#example)
```sh
yarn add @strapi/provider-email-nodemailer
yarn add @cross-solution/strapi-plugin-async-mail
```

## Configuration:

The default configuration is as follows (you must add it to `./config/plugin.js`):

```js
module.exports = {
  // ...
  "async-mail": {
    enabled: true,
    config: {
      scanLimit: 10,
      recipient: "email@example.com",
    },
  },
  // ...
}
```

### Simple mail

A simple mail sets the subject and the text of the mail according to the parameters `subject` and `message`.

```sh
curl -s -d "subject=Mail+Subject&message=Mail+Body+Text" "http://127.0.0.1:1337/async-mail" | jq "."
{
  "id": 60,
  "subject": "Mail Subject",
  "message": "Mail Body Text",
  "status": "new",
  "from": "from@example.com",
  "recipient": "to@example.com",
  "sentAt": null,
  "replyTo": "reply@example.com",
  "priority": null,
  "meta": {},
  "createdAt": "2024-07-10T08:36:10.099Z",
  "updatedAt": "2024-07-10T08:36:10.099Z",
  "isTemplated": false,
  "uid": ""
}
```

### Templated Mail

Mail templates with placeholders can be defined. Async Mail comes with a DEFAULT template, which recognizes the valid placeholders. The values of the placeholders can be passed as POST parameters.

Example
```sh
curl -s -d "subject=subject&text=text&contact=123&message=123&company=1235&phone=123&email=hhj@cross-solution.de&name=test&uid=DEFAULT" http://localhost:1337/async-mail | jq "."
{
  "id": 61,
  "subject": "subject",
  "message": "123",
  "status": "new",
  "from": "from@example.com",
  "recipient": "to@example.com",
  "sentAt": null,
  "replyTo": "reply@example.com",
  "priority": null,
  "meta": {
    "text": "text",
    "contact": "123",
    "company": "1235",
    "phone": "123",
    "email": "hhj@cross-solution.de",
    "name": "test"
  },
  "createdAt": "2024-07-10T08:38:23.415Z",
  "updatedAt": "2024-07-10T08:38:23.415Z",
  "isTemplated": true,
  "uid": "DEFAULT"
}
```

## Collections

collections are created during bootstrap of the plugin

- async-mail-outbox
- async-mail-templates

## License

This plugin is licensed under the MIT License. See the [LICENSE](../LICENSE) file for more information.

  
 
 

 