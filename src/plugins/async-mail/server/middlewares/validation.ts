import generateSchema from "../utils/generateSchema";
import { errors } from "@strapi/utils";

export default (config, { strapi }) => {
  return async (ctx, next) => {
    const { isTemplated, uid } = ctx.request.body;

    if (isTemplated || !!uid) {
      try {
        const template = await strapi.db
          ?.query("plugin::async-mail.async-mail-template")
          .findOne({ where: { uid } });

        if (!template) {
          throw new errors.NotFoundError("Template not found");
        }

        const { params } = template;
        const schema = generateSchema(params);
        const { error } = schema.validate(ctx.request.body);

        if (error) {
          throw new errors.ValidationError("Validation Error", {
            errors: error.details.map((detail) => ({
              message: detail.message.replace(/"/g, ""),
              path: detail.path.join("."),
            })),
          });
        }

        await next();
      } catch (error) {
        throw error;
      }
    } else {
      await next();
    }
  };
};
