import asyncMail from "./async-mail";
import email from "./service";
export default {
  "async-mail": asyncMail,
  email,
};
