import _ from "lodash";
import Mustache from "mustache";
import decode from "decode-html";
import { htmlToText } from "html-to-text";
import { Strapi } from "@strapi/strapi";

const isValidEmail =
  /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

interface EmailOptions {
  to?: string | string[];
  from?: string;
  replyTo?: string;
  [key: string]: any;
}

interface EmailTemplate {
  templateReferenceId?: string;
  text?: string;
  html?: string;
  subject?: string;
  [key: string]: any;
}

export default ({ strapi }: { strapi: Strapi }) => ({
  async sendTemplatedEmail(
    emailOptions: EmailOptions = {},
    emailTemplate: EmailTemplate = {},
    data: object = {}
  ) {
    const keysToIgnore = ["attachment", "attachments", "headers"];

    Object.entries(emailOptions).forEach(([key, address]) => {
      if (!keysToIgnore.includes(key)) {
        if (Array.isArray(address)) {
          address.forEach((email) => {
            if (!isValidEmail.test(email))
              throw new Error(
                `Invalid "${key}" email address with value "${email}"`
              );
          });
        } else {
          if (!isValidEmail.test(address))
            throw new Error(
              `Invalid "${key}" email address with value "${address}"`
            );
        }
      }
    });

    const attributes = ["text", "html", "subject"];

    let bodyHtml: string, bodyText: string, bodySubject: string;

    const { subject = "", text = "", html = "" } = emailTemplate;

    bodyHtml = html.replace(/<%=/g, "{{").replace(/%>/g, "}}");
    bodyText = text.replace(/<%=/g, "{{").replace(/%>/g, "}}");
    bodySubject = subject.replace(/<%=/g, "{{").replace(/%>/g, "}}");

    if ((!bodyText || !bodyText.length) && bodyHtml && bodyHtml.length)
      bodyText = htmlToText(bodyHtml, {
        wordwrap: 130,
      });

    emailTemplate = {
      ...emailTemplate,
      subject:
        (!_.isEmpty(emailTemplate.subject) && emailTemplate.subject) ||
        (!_.isEmpty(bodySubject) && decode(bodySubject)) ||
        "No Subject",
      html: decode(bodyHtml),
      text: decode(bodyText),
    };

    const templatedAttributes = attributes.reduce(
      (compiled, attribute) =>
        emailTemplate[attribute]
          ? Object.assign(compiled, {
              [attribute]: Mustache.render(emailTemplate[attribute], data),
            })
          : compiled,
      {}
    );

    return strapi.plugin("email").provider.send({
      ...emailOptions,
      ...templatedAttributes,
    });
  },
});
