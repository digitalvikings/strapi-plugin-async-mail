import Joi, { ObjectSchema, Schema } from "joi";

interface Attribute {
  name: string;
  type: "string" | "number" | "boolean" | "date" | "object" | "array" | "email";
  default?: any;
}

const typeMap: Record<string, () => Schema> = {
  string: () => Joi.string(),
  number: () => Joi.number(),
  boolean: () => Joi.boolean(),
  date: () => Joi.date(),
  object: () => Joi.object(),
  array: () => Joi.array(),
  email: () => Joi.string().email(),
};

function generateSchema(attributes: Attribute[]): ObjectSchema {
  const schema = attributes.reduce<Record<string, Schema>>((acc, field) => {
    const { name, type, default: defaultValue } = field;

    if (typeMap[type]) {
      let fieldSchema = typeMap[type]();
      if (defaultValue !== undefined) {
        fieldSchema = fieldSchema.default(defaultValue);
      }
      acc[name] = fieldSchema.required();
    } else {
      let fieldSchema = Joi.any();
      if (defaultValue !== undefined) {
        fieldSchema = fieldSchema.default(defaultValue);
      }
      acc[name] = fieldSchema.required();
    }

    return acc;
  }, {});
  
  return Joi.object(schema).options({ stripUnknown: true, abortEarly: false });
}

export default generateSchema;
