const templates = [
  {
    uid: "DEFAULT",
    subject: "Anfrage von <%= data.name %>",
    text: `Neue Anfrage von:
      <%= data.name %>
      <%= data.company %>
      <%= data.email %>
      <%= data.phone %>
      <%= data.contact %>
      <%= data.text %>`,
    recipient: 'please@change.me',
    html: `<h1>Kontakt über DEFAULT</h1>
      <p>
      Name: <%= data.name %><br>
      Firma: <%= data.company %><br>
      Email: <%= data.email %><br>
      Phone: <%= data.phone %><br>
      Contact: <%= data.contact %><br>
      <h2>Nachricht</h2>
      <%= data.text %>.<p>`,
    params: [
      {
        name: "name",
        type: "string",
        default: "",
      },
      {
        name: "email",
        type: "email",
        default: "",
      },
      {
        name: "phone",
        type: "string",
        default: "",
      },
      {
        name: "contact",
        type: "string",
        default: "",
      },
      {
        name: "company",
        type: "string",
        default: "",
      },
      {
        name: "text",
        type: "string",
        default: "",
      },
    ],
  },
];

const UID = "plugin::async-mail.async-mail-template";

export default async (strapi) => {
  for (const template of templates) {
    const entity = await strapi.db.query(UID).findOne({
      where: {
        uid: template.uid,
      },
    });

    if (!entity) {
      await strapi.db.query(UID).create({
        data: template,
      });
    }
  }
};
